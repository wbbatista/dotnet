/*  Exerc�cio 15
    Escreva uma fun��o em pseudoc�digo que receba uma temperatura em Celsius e retorne a sua convers�o em Fahrenheit
    Data: 30/01/2024*/


programa {
funcao inicio() {

    real temperatura

    escreva("escreva uma temperatura em celsis: ")
    leia(temperatura)

    temperatura = temperatura * 1.8 + 32
    escreva("temperatura em Fahrenheit: ", temperatura)
}
}