/*  Exerc�cio 14
    Escreva uma fun��o em pseudoc�digo que receba um n�mero inteiro positivo e retorne o seu fatorial
    Data: 30/01/2024*/

programa {
    funcao inicio() {
        inteiro numero
        inteiro qtddiv = 0

      escreva("digite um n�mero: ")
      leia(numero)

      se (numero > 2){
          para (inteiro i = 1; i <= numero; i++){
            se(numero % i == 0){  
              qtddiv += 1
            }
          }
          se(qtddiv > 2){
            escreva("quantidade de divis�es: ", qtddiv, "\n")
            escreva("n�o � n�mero primo: ", numero)
          }
          senao{
            escreva("quantidade de divis�es: ", qtddiv, "\n")
            escreva("� n�mero primo: ", numero)
          }

      }
      senao{
        se(numero > 0)
          escreva("� n�mero primo: ", numero)
        senao{
          escreva("o n�mero n�o � maior que zero: ", numero)
        }
      }
    }     
}