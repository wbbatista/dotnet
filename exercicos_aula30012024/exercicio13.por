/*  Exerc�cio 13
    Escreva uma fun��o em pseudoc�digo que receba um n�mero inteiro positivo e retorne o seu fatorial
    Data: 30/01/2024*/

programa {

  funcao inicio() {

    inteiro numero
    inteiro result = 1

    escreva("escreva um n�mero inteiro: ")
    leia(numero)
    para(inteiro i = 1; i <= numero; i++){
        result *= i }

    escreva(numero,"! = ", result)
  }
}
