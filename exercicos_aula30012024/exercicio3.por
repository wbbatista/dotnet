/*  Exerc�cio 3
    Escreva um algoritmo em pseudoc�digo que receba um n�mero inteiro positivo e imprima se ele � par ou �mpar
    Data: 30/01/2024*/



programa {

  funcao inicio() {

    inteiro numero
    cadeia result

    escreva("escreva um n�mero: ")
    leia(numero)
    se (numero % 2 != 0){
      result = "�mpar"
    } 
    senao { result = "par"}
      
    escreva("O n�mero �: ", result, "\n")
  }
}
