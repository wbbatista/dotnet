/*  Exerc�cio 6
    Escreva um algoritmo em pseudoc�digo que receba uma matriz de n�meros inteiros e imprima a sua transposta
    Data: 30/01/2024*/



programa {

inclua biblioteca Util --> util

  funcao inicio() {

		// Define as dimens�es (linhas e colunas) da matriz
		inteiro tamanho

    escreva("escreva um n�mero para definir a dimens�o da matriz: ")
    leia(tamanho)
    inteiro matriz[tamanho][tamanho]

    //Atribui um valor aleat�rio � posi��o da matriz
    escreva("\n","Exibe a matriz:","\n") 
    para (inteiro linha = 0; linha < tamanho; linha++)
		{
			para (inteiro coluna = 0; coluna < tamanho; coluna++)
			{
				matriz[linha][coluna] = util.sorteia(1, 10)

        escreva("[", matriz[linha][coluna], "]")
				
			}
      escreva("\n")

    }

    escreva("\n","Exibe a matriz transposta:","\n") 

      // Exibe a matriz transposta
    para (inteiro linha = 0; linha < tamanho; linha++)
		{
			para (inteiro coluna = 0; coluna < tamanho; coluna++)
			{

        escreva("[", matriz[coluna][linha], "]")
				
			}

      escreva("\n")

    }


  }
}
