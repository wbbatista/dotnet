/*  Exerc�cio 4
    Escreva um algoritmo em pseudoc�digo que receba um n�mero inteiro positivo e imprima todos os seus divisores
    Data: 30/01/2024*/



programa {

  funcao inicio() {

    inteiro numero

    escreva("escreva um n�mero: ")
    leia(numero)

    se (numero < 0){
      escreva("O n�mero  deve ser maior igual a 0")
    } 
    senao {
      para (inteiro divisor = 1; divisor <= numero; divisor++) {
        se (numero % divisor == 0) { 
          escreva(divisor, "\n") }
    }
    }

  }
}
