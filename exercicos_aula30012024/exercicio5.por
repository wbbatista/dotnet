/*  Exerc�cio 5
    Escreva um algoritmo em pseudoc�digo que receba um n�mero inteiro positivo e imprima a sua tabuada de multiplica��o
    Data: 30/01/2024*/

programa {

  funcao inicio() {

    inteiro numero

    escreva("escreva um n�mero: ")
    leia(numero)

      para (inteiro i = 0; i <= 10; i++) {
          escreva(numero, "x", i, " = ",numero* i, "\n")
    
    }

  }
}
