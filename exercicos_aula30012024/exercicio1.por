/*  Exerc�cio 1
    Escreva um algoritmo em pseudoc�digo que receba dois n�meros inteiros e imprima a soma deles
    Data: 30/01/2024*/

programa {
  funcao inicio() {
    inteiro numero1, numero2
    escreva("Digite um n�mero: ")
    leia(numero1)
    escreva("Digite outro n�mero: ")
    leia(numero2)

    escreva("A soma dos valores � igual a ", numero1 + numero2)
    
  }
}
