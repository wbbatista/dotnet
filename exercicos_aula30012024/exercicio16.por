/*  Exerc�cio 16
    Escreva uma fun��o em pseudoc�digo que receba dois n�meros inteiros e retorne o seu m�ximo divisor comum
    Data: 30/01/2024*/


programa {
funcao inicio() {

    inteiro num
    inteiro ndois

    escreva("escreva um n�mero: ")
    leia(num)
    escreva("escreva outro n�mero: ")
    leia(ndois)
    escreva("MDC de ", num, " e ", ndois, ": ", mdc(num, ndois))
}

funcao mdc(inteiro num, inteiro ndois){
  se(ndois==0){
    retorne num
  }
  senao{
    retorne mdc(ndois, num % ndois)
  }
}
}


