using System.Reflection;
using Calculadora;

var calc = new Calculo();

  
Console.WriteLine("Qual operação deseja Fazer: ");
Console.WriteLine("1- Adição");
Console.WriteLine("2- Subtração");
Console.WriteLine("3- Multiplicação");
Console.WriteLine("4- Divisão");

int operacao = int.Parse(Console.ReadLine());

Console.WriteLine("Digite o primeiro número:" );
double num1 = double.Parse(Console.ReadLine());

Console.WriteLine("Digite o segundo número:" );
double num2 = double.Parse(Console.ReadLine());

double resultado = calc.Resultado;


switch (operacao)
{
    case 1: {


    resultado = Calculo.Adicao(num1, num2);
    break;
    }
    case 2: {

        resultado = Calculo.Subtracao(num1, num2);
        break;
        }

    case 3: {

        resultado = Calculo.Multiplicacao(num1, num2);
        break;
    }
        case 4: {

            resultado = Calculo.Divisao(num1, num2);
            break;
        }
        default:
            Console.WriteLine("Número Inválido!!!, digite outro número");
            break;
}

Console.WriteLine("O resultado da operação entre {0} e {1} é: {2}", num1 ,num2, resultado);